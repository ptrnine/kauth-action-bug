#pragma once

#include <KAuth/ActionReply>

using KAuth::ActionReply;

class TestActions : public QObject {
    Q_OBJECT
public Q_SLOTS:
    ActionReply testaction(const QVariantMap& args);
};


