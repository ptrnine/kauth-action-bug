#include "kauthbugactions.hpp"

#include <KAuth/HelperSupport>
#include <QDebug>
#include <QThread>

/* Sleeps for 100ms, returns accepted arguments */
ActionReply TestActions::testaction(const QVariantMap& args) {
    qWarning() << "Action called with args:" << args;
    QThread::msleep(100);
    ActionReply reply;
    reply.setData(args);
    return reply;
}

KAUTH_HELPER_MAIN("org.kde.kauth.bug", TestActions)
