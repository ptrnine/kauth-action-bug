cmake_minimum_required(VERSION 3.23.2)
project(kauthbug)

set(QT_MIN_VERSION "5.14.0")
set(KF5_MIN_VERSION "5.85.0")

find_package(ECM ${KF5_MIN_VERSION} CONFIG REQUIRED)
set(CMAKE_MODULE_PATH  ${CMAKE_CURRENT_SOURCE_DIR}/CMake ${ECM_MODULE_PATH})

include(KDEInstallDirs)
include(KDECMakeSettings)
include(KDECompilerSettings NO_POLICY_SCOPE)

find_package(Qt5 ${QT_MIN_VERSION} CONFIG REQUIRED COMPONENTS Core)
find_package(KF5 ${KF5_MIN_VERSION} REQUIRED COMPONENTS Auth)
find_package(PackageKitQt5)

add_definitions(
    -DQT_NO_CAST_TO_ASCII
    -DQT_NO_CAST_FROM_ASCII
    -DQT_NO_URL_CAST_FROM_STRING
    -DQT_NO_CAST_FROM_BYTEARRAY
    -DQT_USE_QSTRINGBUILDER
    -DQT_NO_NARROWING_CONVERSIONS_IN_CONNECT
    -DQT_NO_FOREACH
    -DQT_NO_KEYWORDS
)

kauth_install_actions(org.kde.kauth.bug org.kde.kauth.bug.actions)
add_executable(kauthbugactions kauthbugactions.cpp)
target_link_libraries(kauthbugactions KF5::AuthCore)

kauth_install_helper_files(kauthbugactions org.kde.kauth.bug root)
install(TARGETS kauthbugactions DESTINATION ${KAUTH_HELPER_INSTALL_DIR})

add_executable(kauthbug kauthbug.cpp)
target_link_libraries(kauthbug Qt5::Core KF5::Auth)
