# kauth-action-bug

Something weird happens when the same action is performed multiple times with different arguments - all executions after the first one returns the same result as the first one:

```sh
./build/bin/kauthbug
Start action: "first_run"
Start action: "second_run"
Start action: "third_run"
Warning from helper: Action called with args: QMap(("id", QVariant(QString, "first_run")))
Action result: QMap(("id", QVariant(QString, "first_run")))
Action result: QMap(("id", QVariant(QString, "first_run")))
Action result: QMap(("id", QVariant(QString, "first_run")))
```
