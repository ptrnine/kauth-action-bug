#include <iostream>

#include <QCoreApplication>
#include <QString>
#include <KAuth/Action>
#include <KAuth/ExecuteJob>
#include <QTimer>
#include <QDebug>

class ActionLauncher : public QObject {
    Q_OBJECT
public:
    ActionLauncher(QObject* parent = nullptr): QObject(parent) {}

public Q_SLOTS:
    void run_parallel() {
        start_action(QStringLiteral("first_run"));
        start_action(QStringLiteral("second_run"));
        start_action(QStringLiteral("third_run"));
    }

    void run_sequential() {
        start_action_seq({QStringLiteral("first_run"),
                          QStringLiteral("second_run"),
                          QStringLiteral("third_run")});
    }

Q_SIGNALS:
    void finished();

private:
    int counter = 0;
    void start_action(const QString& id) {
        ++counter;

        KAuth::Action action{QStringLiteral("org.kde.kauth.bug.testaction")};
        action.setHelperId(QStringLiteral("org.kde.kauth.bug"));
        action.addArgument(QStringLiteral("id"), id);

        auto job = action.execute();
        connect(job, &KAuth::ExecuteJob::result, this, [this, job] {
            job->deleteLater();
            qWarning() << "Action result:" << job->data();
            if (--counter == 0)
                Q_EMIT finished();
        });
        qWarning() << "Start action:" << id;
        job->start();
    }

    void start_action_seq(const QStringList& ids) {
        if (ids.empty()) {
            Q_EMIT finished();
            return;
        }

        static constexpr auto drop_front = [](const QStringList& list) -> QStringList {
            if (list.empty())
                return {};
            return {list.begin() + 1, list.end()};
        };

        auto& id = ids.first();
        KAuth::Action action{QStringLiteral("org.kde.kauth.bug.testaction")};
        action.setHelperId(QStringLiteral("org.kde.kauth.bug"));
        action.addArgument(QStringLiteral("id"), id);

        auto job = action.execute();
        connect(job, &KAuth::ExecuteJob::result, this, [this, job, ids] {
            job->deleteLater();
            qWarning() << "Action result:" << job->data();
            start_action_seq(drop_front(ids));
        });
        job->start();
        qWarning() << "Start action:" << id;
    }
};

int main(int argc, char* argv[]) {
    QCoreApplication app(argc, argv);
    auto launcher = new ActionLauncher(&app);
    QObject::connect(launcher, &ActionLauncher::finished, &app, &QCoreApplication::quit);

    QTimer::singleShot(0, launcher, &ActionLauncher::run_parallel);
    //QTimer::singleShot(0, launcher, &ActionLauncher::run_sequential);

    return app.exec();
}

#include "kauthbug.moc"
